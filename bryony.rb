require 'bundler/setup'
Bundler.require(:default)
require 'rubygems'
require './config/config.rb' if File.exists?('./config/config.rb')
require 'sinatra'
require 'sinatra/flash'
require 'pony'
require 'sinatra/static_assets'
require './config/environments/development'
require './config/environments/production'
require './helpers/aws_helper'


enable :sessions



get '/' do 
  erb :index
end

get '/sitemap.xml' do
  content_type 'text/xml'

 
"<urlset>
<url>
  <loc>http://www.bryonygurmin-personaltraining.co.uk/</loc>
</url>
</urlset>"

end

post '/' do
    from = params[:name]
    subject = "#{params[:name]} has contacted you from your Personal Website"
    body = erb(:mail, layout: false)

  Pony.mail(
  :from => from,
  :to => ENV["EMAIL_ADDRESS"],
  :subject => subject,
  :body => body,
  :via => :smtp,
  :via_options => {
    :address              => 'smtp.gmail.com',
    :port                 => '587',
    :enable_starttls_auto => true,
    :user_name            => ENV["USER_NAME"],
    :password             => ENV["PASSWORD"],
    :authentication       => :plain, 
    :domain               => "localhost.localdomain" 
})
  flash[:notice] = "Thanks for your email. I will be in touch soon."
  redirect '/success' 

end

get '/success' do
  erb :index
end